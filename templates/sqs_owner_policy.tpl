{
  "Version": "2012-10-17",
  "Id": "sqs-policy",
  "Statement": [
    {
      "Sid": "owner_statement",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${account_owner_id}"
      },
      "Action": [
          "SQS:*"
        ],
      "Resource": "${sqs_queue_arn}"
    },

    {
      "Sid": "sender_statement",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${account_owner_id}"
      },
      "Action": [
          "SQS:SendMessage"
        ],
      "Resource": "${sqs_queue_arn}"
    },

    {
      "Sid": "receiver_statement",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${account_owner_id}"
      },
      "Action": [
          "SQS:ReceiveMessage",
          "SQS:ChangeMessageVisibility",
          "SQS:DeleteMessage"
        ],
      "Resource": "${sqs_queue_arn}"
    }


  ]
}

