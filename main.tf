
# Module      : SQS
# Description : Terraform module to create SQS resource on AWS for managing queue.

resource "aws_sqs_queue" "this" {
  count = var.create ? 1 : 0

  name        = var.name
  name_prefix = var.name_prefix

  visibility_timeout_seconds  = var.visibility_timeout_seconds
  message_retention_seconds   = var.message_retention_seconds
  max_message_size            = var.max_message_size
  delay_seconds               = var.delay_seconds
  receive_wait_time_seconds   = var.receive_wait_time_seconds
  redrive_policy              = ""
  fifo_queue                  = var.fifo_queue
  content_based_deduplication = var.content_based_deduplication
  deduplication_scope         = var.deduplication_scope
  fifo_throughput_limit       = var.fifo_throughput_limit
  sqs_managed_sse_enabled     = var.sqs_managed_sse_enabled

  //kms_master_key_id                 = aws_kms_key.sqs_key.key_id
  //kms_data_key_reuse_period_seconds = var.kms_data_key_reuse_period_seconds

  tags = var.tags
}

resource "aws_sqs_queue" "dead_letter_queue" {
  count = var.enable_dlq ? 1 : 0

  name        = var.dlq_name
  visibility_timeout_seconds  = var.visibility_timeout_seconds
  message_retention_seconds   = var.message_retention_seconds
  max_message_size            = var.max_message_size
  delay_seconds               = var.delay_seconds
  receive_wait_time_seconds   = var.receive_wait_time_seconds
  fifo_queue                  = true
  content_based_deduplication = false
  deduplication_scope         = var.deduplication_scope
  fifo_throughput_limit       = var.fifo_throughput_limit
  sqs_managed_sse_enabled     = var.sqs_managed_sse_enabled
  redrive_policy             = "{\"deadLetterTargetArn\":\"${var.dlq_arn != "" ? var.dlq_arn : element(concat(aws_sqs_queue.this.*.arn,[""],),0,)}\",\"maxReceiveCount\":${var.max_receive_count}}"

  //kms_master_key_id                 = aws_kms_key.sqs_key.key_id
  //kms_data_key_reuse_period_seconds = var.kms_data_key_reuse_period_seconds

  tags = var.tags
}

/*
resource "aws_kms_key" "sqs_key" {
  description             = "KMS key for sqs"
  deletion_window_in_days = var.deletion_window_in_days
  enable_key_rotation = var.enable_key_rotation
}*/

data "aws_arn" "this" {
  count = var.create ? 1 : 0

  arn = aws_sqs_queue.this[0].arn

}

data "aws_caller_identity" "current" {}

#Create the SQS access policies defined in the template file (AWS_SQS / Templates/)

resource "aws_sqs_queue_policy" "sqs_queue_policy" {
  queue_url                         = aws_sqs_queue.this[0].id
  policy = templatefile("${path.module}/templates/sqs_owner_policy.tpl",
  {
    sqs_queue_arn           = data.aws_arn.this[0].arn
    account_owner_id        = data.aws_caller_identity.current.account_id
  })
}



